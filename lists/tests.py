from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from lists.views import home_page
# Create your tests here.
class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_return_correct_html(self):
        # Test with HttpRequest
        """ request = HttpRequest()
        response = home_page(request)
        html = response.content.decode('utf-8')
        print(html)
        self.assertTrue(html.startswith('<html>'))
        self.assertIn('<title>My todo schedule</title>', html)
        self.assertTrue(html.endswith('</html>'))
        """

        # Test with Django test client
        response = self.client.get('/')
        html = response.content.decode('utf8')
        self.assertTrue(html.startswith('<html lang="en">'))
        self.assertIn('<title>To-Do lists</title>', html)
        self.assertTrue(html.strip().endswith('</html>'))
        self.assertTemplateUsed(response, 'home.html')
