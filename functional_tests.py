# from selenium import webdriver
# from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

# cap = DesiredCapabilities().FIREFOX
# cap["marionette"] = False
# browser = webdriver.Firefox(capabilities=cap, executable_path="C:\\Users\\MZQD9175\\Documents\\projects\\Robotframework\\geckodriver.exe")
# browser.get('http://localhost:8000')

# assert 'To-do' in browser.title

# # All todo list inside application

# browser.quit()


from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
import times
import unittest

class NewVisitorTest(unittest.TestCase):
    # def test_start_with_list(self):
    #     self.browser.get('http://localhost:8000')
    #     self.assertIn('To-Do', self.browser.title)
    #     self.fail('Finish the test!')

    def test_can_starts_with_a_list_and_retrieve_it_later(self):
        # Test homepage
        self.browser.get('http://localhost:8080')

        # Test for page title
        self.assertIn('To-Do', self.browser.title)

        # Test the header element
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # Test for input element
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(inputbox.get_attribute('placeholder'), 'Enter a to-do item')

        # Selenium way od typing into input elements
        inputbox.send_keys("Hello, this is a test")

        # When hits enter, the page updates and page list also updates
        inputbox.send_keys(Keys.ENTER)
        # Explicit wait, because when we hit ENTER, to avoid browser refresh, wait.
        times.sleep(1)

    def setUp(self):
        cap = DesiredCapabilities().FIREFOX
        cap["marionette"] = False
        self.browser = webdriver.Firefox(capabilities=cap,
                                         executable_path="C:\\Users\\MZQD9175\\Documents\\projects\\django\\TDD_with_Django\\geckodriver.exe")
    
    def tearDown(self):
        self.browser.quit()

if __name__ == '__main__':
    unittest.main(warnings='ignore')
